from django.apps import AppConfig


class AmazonAppConfig(AppConfig):
    name = 'Amazon_app'
