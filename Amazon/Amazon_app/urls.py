from django.urls import path,include
from rest_framework import routers
from Amazon_app.rest_views import UserViewSet,ProductViewset,OrdersViewset,OrderItemViewset
from rest_framework.authtoken.views import obtain_auth_token

router = routers.DefaultRouter()
router.register('api/register', UserViewSet,)
router.register('api/products',ProductViewset)
router.register('api/orders',OrdersViewset,basename="Orders")
router.register('api/order_items',OrderItemViewset)

urlpatterns=[
    path('',include(router.urls)),
    path('api/login/',obtain_auth_token)
]
