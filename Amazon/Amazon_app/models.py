from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class Products(models.Model):
    title=models.CharField(max_length=30)
    description = models.TextField(max_length=100)
    image=models.ImageField(upload_to='product/images')
    price=models.IntegerField()
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)

    def __str__(self):
        return f'{self.title}'


class Orders(models.Model):
    payment_options = (
        ("cash", "Cash"),
        ("paytm", "Paytm"),
        ("card", "Card"),

    )
    status_options=(
        ('new','New'),
        ('paid','Paid')
    )

    user_id=models.ForeignKey(User,on_delete=models.CASCADE)
    products = models.ManyToManyField(Products)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    status= models.CharField(max_length=20,choices=status_options)
    mode_of_payment=models.CharField(max_length=20,choices=payment_options)
    total = models.IntegerField()



    def __str__(self):
        return f"{self.id}"

class OrdersItems(models.Model):

    user_id=models.ForeignKey(User,on_delete=models.CASCADE,default=None)
    product_id=models.ForeignKey(Products,on_delete=models.CASCADE)
    quantity=models.IntegerField()

    def price(self):
        return self.product_id.price

    def final_price(self):
        print('product_id price:',self.product_id.price)
        cost=self.quantity * self.product_id.price
        print('cost:',cost)
        return cost

    def __str__(self):
        return f"{self.product_id}"
