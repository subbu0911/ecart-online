from django.contrib.auth.models import User


from rest_framework.authentication import  TokenAuthentication
from rest_framework import viewsets, status


from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.filters import SearchFilter,OrderingFilter

from Amazon_app.models import Orders,Products,OrdersItems
from Amazon_app.serializers import UserSerializer,ProductSerializer,OrdersSerializer,OrderItemSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class ProductViewset(viewsets.ModelViewSet):
    queryset = Products.objects.all()
    serializer_class = ProductSerializer
    filter_backends = (SearchFilter, OrderingFilter)
    search_fields = ('id', 'title')
    # authentication_classes = [TokenAuthentication]
    # permission_classes = [IsAuthenticated]



class OrdersViewset(viewsets.ModelViewSet):
    queryset = Orders.objects.all()
    serializer_class = OrdersSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]


    def list(self, request):
        # queryset = User.objects.all()
        orders_data =Orders.objects.filter(user_id = request.user)
        serializer = OrdersSerializer(orders_data, many=True)
        return Response(serializer.data)

    def create(self, request,  *args, **kwargs):
        data = request.data

        new_order = Orders.objects.create(user_id=request.user,total=data['total'],status=data['status'],mode_of_payment=data['mode_of_payment'])
        new_order.save()

        for product in data['products']:
            product_data =Products.objects.get(title=product['title'])
            new_order.products.add(product_data)
        serializer = OrdersSerializer(new_order)

        return Response(serializer.data)

    def retrive(self, request, pk=None):
        queryset = Orders.objects.filter(pk=pk,user_id=request.user, )
        if not queryset:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = OrdersSerializer(queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)




    def partial_update(self, request, pk=None):
        orders= Orders.objects.get(user_id=request.user,pk=pk)
        data = request.data

        try:
            for product in data['products']:
                product_obj = Products.objects.get(title=product['title'])

                orders.products = product_obj
        except Exception:
            print('error',Exception)

        orders.total =data.get('total',orders.total)
        orders.status = data.get('status', orders.status)
        orders.mode_of_payment = data.get('mode_of_payment', orders.mode_of_payment)
        orders.save()

        serialized = OrdersSerializer(request.user, data=request.data, partial=True)
        return Response(serialized.data,status=status.HTTP_202_ACCEPTED)





class OrderItemViewset(viewsets.ModelViewSet):
    queryset = OrdersItems.objects.all()

    serializer_class = OrderItemSerializer
    authentication_classes =  [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        data = request.data

        new_product = Products.objects.get(title=data['product_id'])
        new_product.save()
        new_order_items = OrdersItems.objects.create(product_id = new_product,quantity=data['quantity'])
        new_order_items.save()
        serializer = OrdersItems(new_order_items)

        return Response(serializer.data)